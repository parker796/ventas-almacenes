<?php
   
    session_start(); //marcamos la sesion
    require_once "../../clases/conexion.php";
	require_once "../../clases/categorias.php";
    $fecha = date("Y-m-d"); //esta extrai desde el sistema cuando agregas algo se pone en mayuscula para el año completo si lo ponemos en minusculas sale no mas cortado 20
    //agregamos el id de la sesion del usuario que se mantiene abierta seguimos el mismo hilo del usuario
    $idusuario = $_SESSION['iduser'];
    $categoria = $_POST['categoria'];

    $datos = array( $idusuario,
                    $categoria,
                    $fecha );

    $obj = new categorias();
    echo $obj->agregarCategorias($datos); //le mandamos los datos que se insertaron en el formulario
?>