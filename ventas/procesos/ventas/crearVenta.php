<?php
    session_start();
	require_once "../../clases/conexion.php";
	require_once "../../clases/ventas.php";
	$obj = new ventas();
	if(count($_SESSION['tablaComprasTemp']) == 0){ //cuenta las sesiones de la tabla temporal que en este caso son las compras
		echo 0; //nos devuelve 0
	}else{
		$result = $obj->crearVenta(); //TRAEMOS LAS VENTAS HECHAS
		unset($_SESSION['tablaComprasTemp']); //quitamos la sesion de la tabla
		echo $result; //devolvemos las ventas hechas
	}
?>