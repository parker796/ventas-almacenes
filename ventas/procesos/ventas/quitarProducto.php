<?php
    session_start();
    $index = $_POST['ind'];
    unset($_SESSION['tablaComprasTemp'][$index]); //esto es en la posicion que nos mandaron que quiso borrar
    //hay que reindexar las posiciones por que silo quito y lo iteramos no va ser congruente van a quedar posiciones vacias y lo que necesitamos es otra vez 
    //forma las posiciones adecuadas y ya con esta funcion quedan las posiciones arregladas algo asi como un arreglo dinamico en c++
    $datos = array_values($_SESSION['tablaComprasTemp']);
    unset($_SESSION['tablaComprasTemp']); //volvemos a borrar la sesion que se hizo
    $_SESSION['tablaComprasTemp'] = $datos; //y le mandamos a la sesion los nuevos valores indexados
?>