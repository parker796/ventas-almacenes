<?php
	require_once "clases/conexion.php";
	$obj = new conectar();
	$conexion = $obj->conexion();
    //esta parte de la validsacion del administrador nos ayuda a ya no registrarse mas de otro administrador
	$sql = "select * from usuarios where email = 'admid'";
	$result = mysqli_query($conexion,$sql);
	if( mysqli_num_rows($result) > 0 ){
		header("location:index.php"); //esta parte es para que ya no se registre otro administrador y ya no entra al registro
	}

?>
<!Doctype html>
<head>
        <title>registro</title>
        <link rel="stylesheet" type="text/css" href="librerias/bootstrap/css/bootstrap.css">
	    <script src="librerias/jquery-3.2.1.min.js"></script>
        <script src="js/funciones.js"></script>
</head>
<body>
<br><br><br>
    <div class="container" >
    <div class="row">
        <div class="col-sm-4"> </div>
        <div class="col-sm-4"> 
            <div class="panel panel-danger"> 
                <div class="panel panel-heading">Registrar Administrador</div>
                <div class="panel panel-body">
                     <form id="frmRegistro">
                            <label>Nombre</label>
                            <input type="text" class="form-control input-sm" name="nombre" id="nombre">
                            <label>Apellido</label>
                            <input type="text" class="form-control input-sm" name="apellido" id="apellido">
                            <label>Usuario</label>
                            <input type="text" class="form-control input-sm" name="usuario" id="usuario">
                            <label>Password</label>
                            <input type="text" class="form-control input-sm" name="password" id="password">
                        <p></p>
                        <span class="btn btn-primary" id="registro">Registrar</span>
                        <a href="index.php" class="btn btn-default">regresar login</a>
                     </fom>
                </div>
            </div>
        </div>
        <div class="col-sm-4"> </div>

    </div>
    </div>
</body>
</html>

<script type="text/javascript">
//creamos el vento del boton registro junto con el formulario que leemos 
    $(document).ready(function(){
        $('#registro').click(function(){
            //si por alguna razon hubo un campo vacio retorna un contador
            vacios = validarFormVacio('frmRegistro');

            if( vacios > 0 ){
                alert("Debes de llenar todos los campos");
                return false; //esto es para que no siga el proceso de ajax
            }


            datos = $('#frmRegistro').serialize();
            //datos = $('#frmRegistro').serialize();
            $.ajax({
                type:"POST",
                data:datos,
                url:"procesos/regLogin/registrarUsuarios.php",
                success:function(r){
                    alert(r);
                    if( r == 1){ //si todo estuvo correcto
                        alert("Agregando con exito");
                    }else{
                        alert("Fallo agregar :(");
                    }
                }
              
            });
        });
    });
</script>