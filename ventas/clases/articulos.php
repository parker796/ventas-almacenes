<?php
    class articulos{
        //creamos nuestro metodo para agregar la imagen a la BD. id de la categoria y su ruta lo mas importante
        public function agregarImagenes($datos){
            $c = new conectar();
            $conexion = $c->conexion();
            $sql = "INSERT INTO imagenes(id_categoria, 
                                    nombre, 
                                    ruta, 
                                    fechaSubida)
                    VALUES('$datos[0]', 
                            '$datos[1]', 
                            '$datos[2]', 
                            '$datos[3]'
                            )";
            $result = mysqli_query($conexion,$sql);
            return mysqli_insert_id($conexion); //nos devuelve el ultimo id agregado de imagenes a travez de la conexion
        }
        //metodo para agregar articulos ya con el id de la imagen
        public function insertaArticulo($datos){
			$c = new conectar();
			$conexion = $c->conexion();
			$sql="INSERT INTO articulos (id_categoria,
										id_imagen,
										id_usuario,
										nombre,
										descripcion,
										cantidad,
										precio,
										fechaCaptura) 
							values ('$datos[0]',
									'$datos[1]',
									'$datos[2]',
									'$datos[3]',
									'$datos[4]',
									'$datos[5]',
									'$datos[6]',
									'$datos[7]')";
			return mysqli_query($conexion,$sql);
        }
        
        public function obtenDatosArticulo($idarticulo){
			$c=new conectar();
			$conexion=$c->conexion();

			$sql="SELECT id_producto, 
						id_categoria, 
						nombre,
						descripcion,
						cantidad,
						precio 
				from articulos 
				where id_producto='$idarticulo'";
			$result=mysqli_query($conexion,$sql);

			$ver=mysqli_fetch_row($result);

			$datos=array(
					"id_producto" => $ver[0],
					"id_categoria" => $ver[1],
					"nombre" => $ver[2],
					"descripcion" => $ver[3],
					"cantidad" => $ver[4],
					"precio" => $ver[5]
						);

			return $datos;
		}

		public function actualizaArticulos($datos){
			$c = new conectar();
			$conexion = $c->conexion();
			$sql = "UPDATE articulos set id_categoria = '$datos[1]', 
										  nombre = '$datos[2]',
										  descripcion = '$datos[3]',
										  cantidad = '$datos[4]',
										  precio = '$datos[5]' where id_producto = '$datos[0]' ";
			return mysqli_query($conexion,$sql); //esto manda uno de verdadero si se hace correctamente
		}
		public function eliminarArticulos($idart){
			$c = new conectar();
			$conexion = $c->conexion();

			//obtenemos el id de la imagen desde productos porque son igual son llaves primarias tiene id igual
			$idimagen = self::obtenIdImg($idart); //es como el this de java para traer el metodo

			$sql = "DELETE FROM articulos WHERE id_producto = '$idart'";
			$result = mysqli_query($conexion,$sql); //devuelve 1 de verdadero
				if($result){ //si no es igual a null pasa
					//obtenemos la ruta en base al id que obtuvimos
					$ruta = self::obtenRutaImagen($idimagen);
					$sql = "DELETE FROM imagenes WHERE id_imagen = '$idimagen'"; //borramos el registro de la imagen
					$result = mysqli_query($conexion,$sql); 
						if($result){ //si se cumplio la consulta
							if(unlink($ruta)){ //unlink sirve para borrar archivos entonces por eso mandamos la ruta
								//esto esta correcto porque se manda a llamar dentro de dos carpetas 
								return 1; //para que entre al if del articulo y cargue la tabla como lo venimos manejando
							}
						}
			}
		}
		//aqui necesitamos tanto id que se guardo en la tabla articulos como la ruta 
		public function obtenIdImg($idProducto){
			$c = new conectar();
			$conexion = $c->conexion();

			$sql ="SELECT id_imagen 
					from articulos 
					where id_producto='$idProducto'";
			$result = mysqli_query($conexion,$sql);

			return mysqli_fetch_row($result)[0]; //recorro la fila de datos de la consulta pero nos manda el de la posicion 0 por que no queremos todos
		}

		public function obtenRutaImagen($idImg){
			$c = new conectar();
			$conexion = $c->conexion();

			$sql = "SELECT ruta 
					from imagenes 
					where id_imagen='$idImg'";

			$result=mysqli_query($conexion,$sql);

			return mysqli_fetch_row($result)[0]; //recorro el resultado de la consulta pero es 0 porque es el primero que nos muestra por el parametro id
		}


    }
?>