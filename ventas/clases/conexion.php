<?php 
    class conectar{
        //definimos nuestras variables para la conexion
        //solamente accedemos dentro de esta clase encapsulamos la conexion
        private $servidor = "localhost";
        private $usuario = "root";
        private $password = "";
        private $bd = "ventas";
        //accedemos a las variables privadas como en java con this
        public function conexion(){
            $conexion = mysqli_connect($this->servidor,
                                      $this->usuario,
                                      $this->password,
                                      $this->bd);
            return $conexion;
        }

    }
    //hacemos una prueba de funcionamiento
   /* $obj = new conectar(); //instanciamos la clase genera su constructor por default
    //imprimos el estado si hubo una conexion correcta o n
    //var_dump($obj->conexion()); //se puede usar echo igual pero manejamos var_dump
    if($obj->conexion()){
        echo "conexion satisfactoria";
    }  */ //vemos que fue correcto lo comentamos

    
?>