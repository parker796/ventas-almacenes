<?php
//todo lo que tiene que ver con usuarios altas,bajas,eliminaciones etc
    class usuarios{
        public function registroUsuario($datos){
            $c = new conectar();
            $conexion = $c->conexion();
            $fecha = date('Y-m-d');
            //generamos el sql
            $sql = "INSERT INTO usuarios (nombre,
                                apellido,
                                email,
                                password,
                                fechaCaptura)
                        values('$datos[0]',
                                '$datos[1]',
                                '$datos[2]',
                                '$datos[3]',
                                '$fecha')";
            return mysqli_query($conexion, $sql);
        }

        public function loginUser($datos){
            $c = new conectar();
            $conexion = $c->conexion();

            //crearemos la sesion del usuario
            $_SESSION['usuario'] = $datos[0]; //que es el nombre de email que nos confundimos que es el usuario en BD
            //aqui mandamos a traer el metodo para obtener el id del usuario
            $_SESSION['iduser'] = self::traerID($datos); //esto es parecido a c++ con las clases
            
            //ya tenemos al usuario como al id
            //aqui igual podemos cifrar la contraseña como queramos
            $password = sha1($datos[1]);
            $sql = "select * from usuarios
                    where email='$datos[0]'
                    and password='$password'";
            $result = mysqli_query($conexion,$sql);
            //ahora nos preguntamos si fue encontrado lo que 
            //nos devolvio result si encontro las coincidencias
            if(mysqli_num_rows($result) > 0 ){
                return 1;
            }else{
                return 0;
            }
        }

        //vamos a traer el id del usuario
        public function traerID($datos){
            $c = new conectar();
            $conexion = $c->conexion();
            //no necesitamos hacer lo del password porque lo hacemos desde login pero igual puede ser una mala practica
            //aunque necesitaremos testear esa parte si no lo hacemos aca igual el cifrado
            $password = sha1($datos[1]);
            $sql = "select id_usuario from usuarios 
                            where email='$datos[0]'
                            and password='$password'";

            $result = mysqli_query($conexion,$sql);

            return mysqli_fetch_row($result)[0]; //esto nos traia el primero elemento del arreglo que es result
        }
        //esta es la funcion para agregar usuarios dentro del usuario administrador
        public function administraUsuarios($datos){
            $c = new conectar();
            $conexion = $c->conexion();
            $fecha = date('Y-m-d');
            $password = sha1($datos[3]); //encriptamos la contraseña
            $sql = "INSERT INTO usuarios (nombre,
                                          apellido,
                                          email,
                                          password,
                                          fechaCaptura)
                                        values('$datos[0]',
                                                '$datos[1]',
                                                '$datos[2]',
                                                '$password',
                                                '$fecha')";
            return mysqli_query($conexion, $sql);
        }

        //esta funcion es para obtener los datos y mostralos en el modal a la ahora de editar
        public function obtenDatosUsuarios($idart){
            $c = new conectar();
            $conexion = $c->conexion();

            $sql = "SELECT id_usuario, nombre, apellido, email FROM usuarios 
            WHERE id_usuario = '$idart'";
            $result = mysqli_query($conexion,$sql);
            $ver = mysqli_fetch_row($result);

            $datos = array(
                "id_usuario" => $ver[0],
                "nombre" => $ver[1],
                "apellido" => $ver[2],
                "usuario" => $ver[3] );
            
                return $datos;
        }
        //funcion para crear la actualizacion de los usuarios
        public function actualizaUsuarios($datos){
            $c = new conectar();
            $conexion = $c->conexion();

            $sql = "UPDATE usuarios set  nombre = '$datos[1]',
										  apellido = '$datos[2]',
										  email = '$datos[3]' where id_usuario = '$datos[0]' ";
			return mysqli_query($conexion,$sql); //esto manda uno de verdadero si se hace correctamente
          
        }
        public function eliminarUsuarios($idusua){
            $c = new conectar();
            $conexion = $c->conexion();

            $sql = "DELETE FROM usuarios WHERE id_usuario = '$idusua' ";
			return mysqli_query($conexion,$sql); //esto manda uno de verdadero si se hace correctamente
        }
    }
?>