<?php
    class ventas{
        //la primera parte es traer los productos que seleccinamos
        public function llenarProductos($idproducto){
            $c = new conectar();
            $conexion = $c->conexion();
            $sql = "SELECT  art.nombre, 
                            art.descripcion, 
                            art.cantidad, 
                            img.ruta,
                            art.precio
                            FROM articulos AS art 
                            INNER JOIN imagenes as img
                            on  art.id_imagen = img.id_imagen and art.id_producto = '$idproducto'";
                           
            $result = mysqli_query($conexion,$sql);
            $ver = mysqli_fetch_row($result);
            //dividimos la cadena en base a los parentesis que son nuestros limites
            $d = explode('/', $ver[3]); //vamos a quitar una ruta porque no necesitamos la doble dependiendo de donde nos encontremos
            $img = $d[1].'/'.$d[2].'/'.$d[3];
            
            $datos = array( "nombre" => $ver[0],
                            "descripcion" => $ver[1],
                            "cantidad" => $ver[2],
                            "ruta" => $img,
                            "precio" => $ver[4] );
            return $datos;
        }
       //creamos la venta y esa se va guardar en BD en base a la tabla temporal que hicimos
        public function crearVenta(){
            $c = new conectar();
            $conexion = $c->conexion();
            $fecha = date('Y-m-d');
            $idventa = self::creaFolio(); //traremos el folio que generamos para i-esima venta
            $datos = $_SESSION['tablaComprasTemp'];
            $idusuario = $_SESSION['iduser']; //este el id del usuario en la que estamos haciendo la sesion
            $r = 0;
            for( $i = 0; $i < count($datos); $i++){
                $d = explode("||", $datos[$i]);
                //esta inserccion viene de como se trajeron los datos de la sesion de la tabla temporal a que mandamos hacer
               /* $idproducto."||".
				$nombreproducto."||".
				$descripcion."||".
				$precio."||".
				$ncliente."||".
				$idcliente;*/
                $sql = "INSERT INTO ventas (id_venta,
                                            id_cliente,
                                            id_producto,
                                            id_usuario,
                                            precio,
                                            fechaCompra ) 
                                VALUES ( '$idventa', 
                                          '$d[5]',
                                          '$d[0]',
                                          '$idusuario',
                                          '$d[3]',
                                          '$fecha' )";
                
                $r = $r + $result = mysqli_query($conexion,$sql);
            }
            return $r; //devolvemos las ventas que se hicieron que fueron las insercciones a BD
        }
        //esto es porque al crear las ventas de un producto se debe de generar con su mismo 
        //id por eso no se creo como llave primaria. al igual de que las ventas almacenadas para la tabla temporal
        //se generan para ese mismo id no cambia hasta que se vuelve a realizar otra por eso no se pone en llave 
        //primaria porque lo tendrias que definir como auto incrementable y cambiara el id por cada venta que es la mismma
        //si se realiza por eso se maneja asi y el folio nos sirve para obtener esos id que agrupamos en la tabla temporal
        //y generar su reporte con un ticket
        public function creaFolio(){
            $c = new conectar();
            $conexion = $c->conexion();
            //group by nos agrupa las ventas que son repetidas en una sola
            $sql = "SELECT id_venta from ventas group by id_venta desc"; //agrupamos las ventas de una forma descendente o igual puede ser ascendente depende de las necesidades
    
            $resul = mysqli_query($conexion,$sql);
            $id = mysqli_fetch_row($resul)[0];
    
            if($id == "" or $id == null or $id == 0){
                return 1;
            }else{
                return $id + 1; //hacemos un incremento para cada venta que se vaya generando en esa agrupacion del mismo id de la venta
            }
        }
        //esta es para las ventas hechas que se hicieron necesitamos dos consultas 
        public function nombreCliente($idCliente){
            $c = new conectar();
            $conexion = $c->conexion();
    
             $sql="SELECT apellido,nombre 
                from clientes 
                where id_cliente = '$idCliente'";
            $result = mysqli_query($conexion,$sql);
    
            $ver = mysqli_fetch_row($result); //este consulta puede ser 0 porque no hubo registro de un cliente
            //y la compra se hizo por eso se puso estar parte
            //pero puedue que no haya clientes registrados igual entonces 
            //devolvemos igual un null igual podemos vender sin necesidad que sea un cliente por eso devuelve null
            return $ver[0]." ".$ver[1]; //concatenamos el resultado
        }
    
        public function obtenerTotal($idventa){
            $c = new conectar();
            $conexion = $c->conexion();
    
            $sql = "SELECT precio 
                    from ventas 
                    where id_venta = '$idventa'"; //hace la busqueda de las ventas que se agruparon y me devuelve en total de eas ventas
            $result = mysqli_query($conexion,$sql);
    
            $total = 0;
    
            while($ver = mysqli_fetch_row($result)){ //mientras existan resultados leidos
                $total = $total + $ver[0]; //se le suma al total de la venta
            }
    
            return $total;
        }
}
?>