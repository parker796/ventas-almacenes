<?php
	require_once "clases/conexion.php";
	$obj = new conectar();
	$conexion = $obj->conexion();
//nos sirve para validar al administrador si esto existe ya no muestra el boton de registrar
	$sql = "select * from usuarios where email = 'admid'";
	$result = mysqli_query($conexion,$sql);
	$validar = 0;
	if( mysqli_num_rows($result) > 0 ){
		$validar = 1;
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>login de usuarios</title>
    <link rel="stylesheet" type="text/css" href="librerias/bootstrap/css/bootstrap.css">
	<script src="librerias/jquery-3.2.1.min.js"></script>
	<script src="js/funciones.js"></script>
</head>
<body>
	<br><br><br>
	<div class="container">
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<div class="panel panel-primary">
					<div class="panel panel-heading">Sistema de ventas y almacen</div>
					<div class="panel panel-body">
						<p>
                        <img src="img/ventas.jpg"  height="100">
                        </p>
                        <form id="frmLogin">
							<label>Usuario</label>
							<input type="text" class="form-control input-sm" name="usuario" id="usuario">
							<label>Password</label>
                            <input type="password" name="password" id="password" class="form-control input-sm">
                            <p></p>
							<span class="btn btn-primary btn-sm" id="entrarSistema">Entrar</span>
							<?php if(!$validar): /*si no es verdadero validar muestra el registro para registrar un admid*/?> 
							<a href="registro.php" class="btn btn-danger btn-sm">Registrar</a>
							<?php /*si ya existe un admid ya no muestra el boton de registro*/endif; ?>
						</form>
                    </div>
				</div>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</div>
</body>
</html>

<script type="text/javascript">
//creamos el evento para el boton entrar y el formulario
	$(document).ready(function(){
		$('#entrarSistema').click(function(){
           
		   //si por alguna razon hubo un campo vacio retorna un contador
		   vacios = validarFormVacio('frmLogin');

			if( vacios > 0 ){
				alert("Debes de llenar todos los campos");
				return false; //esto es para que no siga el proceso de ajax
				}


            datos = $('#frmLogin').serialize();
            //datos = $('#frmRegistro').serialize();
            $.ajax({
                type:"POST",
                data:datos,
                url:"procesos/regLogin/login.php",
                success:function(r){
                    if( r == 1){ //si todo estuvo correcto
                        window.location = "vistas/inicio.php";
                    }else{
                        alert("no se pudo acceder :(");
                    }
                }
              
            });
        });
	});

</script>