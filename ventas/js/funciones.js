//lo ocupamos para validarFormularioVacio
function validarFormVacio(formulario) {
    datos = $('#' + formulario).serialize();
    d = datos.split('&');
    vacios = 0;
    for (i = 0; i < d.length; i++) {
        controles = d[i].split("="); //A es un valor por defecto nulo A como los select por eso se pone asi el algoritmo
        if (controles[1] == "A" || controles[1] == "") {
            vacios++;
        }
    }
    return vacios;
}