<?php
    //generacion de las ventas hechas
    require_once "../../clases/conexion.php";
    require_once "../../clases/ventas.php";
    $c = new conectar();
    $conexion = $c->conexion();
    $obj = new ventas();
    $sql = "SELECT id_venta, fechaCompra, id_cliente FROM ventas GROUP BY id_venta"; //traemos agrupados a la venta que se hizo para que se pueda imprimir y luego se pasa al total de las ventas
    $result = mysqli_query($conexion,$sql);
    //nuestro folio es el id de la venta que se genero para n-productos en la tabla temporal y que se guardo en BD
?>

<table class="table table-hover table-condesed table-bordered" style="text-align: center;">
<caption><label>Reportes y ventas</label></caption>
    <tr>
    <td>Folio</td>
    <td>Fecha</td>
    <td>Cliente</td>
    <td>Total de compra</td>
    <td>Ticket</td>
    <td>Reporte</td>
    </tr>
    <?php 
        while( $ver = mysqli_fetch_row($result)):
    ?>
    <tr>
    <td><?php echo $ver[0] ?></td>
    <td><?php echo $ver[1] ?> </td>
    <td><?php   if($obj->nombreCliente($ver[2]) == " "){ //si hubo cliente devuelve el nombre del cliente si no devuelve no es cliente
                    echo "Este no es un cliente";
                }else{
                    echo $obj->nombreCliente($ver[2]);
                }
        ?>
    </td>
    <td><?php   echo "$".$obj->obtenerTotal($ver[0]); //recordando que ya tenemos el id de la venta
        ?>
    </td>
    <td><!--el gato es para que no haga nada y se quede ahi-->
     <a href="../vistas/ventas/ventaTicket.php?idventa=<?php echo $ver[0] ?>" class="btn btn-primary btn-sm">
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-printer-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path d="M5 1a2 2 0 0 0-2 2v1h10V3a2 2 0 0 0-2-2H5z"/>
            <path fill-rule="evenodd" d="M11 9H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1z"/>
            <path fill-rule="evenodd" d="M0 7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-2a2 2 0 0 0-2-2H5a2 2 0 0 0-2 2v2H2a2 2 0 0 1-2-2V7zm2.5 1a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
        </svg>
      </a>
    </td>
    <td><!--reporte PDF se agrega la ruta y el sigo de ?idventa=y el php donde nosotros tenemos un identificador para generar el pdf de cada uno -->
    <a href="../vistas/ventas/reporteVentapdf.php?idventa=<?php echo $ver[0] ?>" class="btn btn-primary btn-sm">
    <!--estos iconos viene en la documentacion de bootstrap recordando que tenemos archivo de dependecias-->
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-file-text-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M12 1H4a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2zM5 4a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm-.5 2.5A.5.5 0 0 1 5 6h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zM5 8a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm0 2a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1H5z"/>
        </svg>
    </a>
    </td>
    </tr>
    <?php
        endwhile;
    ?>
</table>