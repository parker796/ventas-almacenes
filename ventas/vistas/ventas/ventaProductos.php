<?php
    //para mostrar las ventas del producto del x cliente
    require_once "../../clases/conexion.php";
    $c = new conectar();
    $conexion = $c->conexion();


?>

<h4>Vender un Producto</h4>
<div class="row">
    <div class="col-sm-4">
        <form id="frmVentasProductos">
            <label>Selecciona Cliente</label>
            <select class="form-control input-sm" name="cliente" id="cliente">
            <option value="A">Selecciona</option>
            <option value="0">sin cliente</option> <!--esta parte si es valida porque puedue que no seamos clientes pero aun asi se puede hacer una venta
            esto se pone con un id de cero siempre en su caso o se puede cambiar por 1 etc pero que sea indentifcable de que no es cliente-->
            <?php //para jalar los datos a la hora de seleccionar
                $sql = "SELECT id_cliente, nombre, apellido FROM clientes";
                $result = mysqli_query($conexion,$sql);
                while($ver = mysqli_fetch_row($result)): //leemos el resultado de la consulta por filas
            ?>          <!--. " ". es para concatenar en php mostramos por apellido y nombre el value es el id que seleccionamos n base al cliente -->
            <option value="<?php echo $ver[0] ?>"><?php echo $ver[2]." ".$ver[1]  ?> </option>
            <?php 
                endwhile;
            ?>
            </select>
            <label>Producto</label>
            <select class="form-control input-sm" name="producto" id="producto">
                <option value="A">Selecciona</option>
                <!-- <option value="0">sin productos</option> esta parte no es valida por que estamos validanddo
                los productos por eso ya no va esto aca -->
                <?php 
                    $sql = "SELECT id_producto, nombre FROM articulos";
                    $result = mysqli_query($conexion,$sql);
                    while($ver = mysqli_fetch_row($result)):
                ?>
                <option value="<?php echo $ver[0] ?>"><?php echo $ver[1] ?></option>
                <?php
                endwhile;
                ?>
            </select>
            <label>Descripcion</label> <!-- readonly es de html 5 y solo leemos en lectura-->
            <textarea readonly="" class="form-control input-sm" id="descripcion" name="descripcion"></textarea>
            <label>Cantidad</label>
            <input readonly="" type="text" class="form-control input-sm" name="cantidad" id="cantidad">
            <label>Precio</label>
            <input readonly=""  type="text" class="form-control input-sm" name="precio" id="precio">
            <pS</p>
            <span class="btn btn-primary" id="btnAgregarVenta">Agregar </span>
            <span class="btn btn-danger" id="btnVaciarVenta">Vaciar ventas </span>
        </form>
     </div>
    <div class="col-sm-3">
        <div id="imgProducto"></div>
    </div>
    <div class="col-sm-4">
		<div id="tablaVentasTempLoad"></div>
	</div>
</div>
<script type="text/javascript">
    var n = 0; //esta se inicia en 0 en cuanto abre la pestaña de la pagina
    //este es para cuando nosotros seleccionamos un producto en el select se muestre lo demas
    $(document).ready(function(){  //en cuanto se detecta los eventos en la pagina la variable global no se ve
    //afectada mas que en este ciclo del programa
        //esta es la tabla de cargar venta
        $('#tablaVentasTempLoad').load("ventas/tablaVentasTemp.php");
        
        $('#producto').change(function(){ //este es como click pero cuando se haga el cambio del producto se muestre el cambio
            //alert("hola");
            //generamos nuestro ajax
            if( n == 1 ){//al primer cambio todo esta bien se agrega una imagen no entramos a estar parte
                $('#imgProducto').empty(); //este sirve para vaciar los nodos hijos pero no el padre
                //remove si elimina todo los elementos y ya no se puede hacer nada
                n = 0;//por costumbre
            }
            $.ajax({
                type:"POST",
                data:"idproducto=" + $('#producto').val(), //asi ya mandamos el id del producto que contiene su id
                url:"../procesos/ventas/llenarFormProductos.php",
                success:function(r){
                  // alert(r); //para testar
                   datos = jQuery.parseJSON(r);
                   $('#descripcion').val(datos['descripcion']);
                   $('#cantidad').val(datos['cantidad']);
                   $('#precio').val(datos['precio']); //el prepend sirve para insertar contenido img-thumnail es de bootstrap
                   $('#imgProducto').prepend('<img class="img-thumbnail" id="imgP" src="' + datos['ruta'] + '"/>');
                   n = 1; //se llega agregar esta imagen en primera instancia y activamos nuestro centinela
                }
            });
        });
        $('#btnAgregarVenta').click(function(){
            //validamos el formulario
			vacios = validarFormVacio('frmVentasProductos');
			if(vacios > 0){
				alertify.alert("Debes llenar todos los campos!!");
				return false;
			}

			datos = $('#frmVentasProductos').serialize();
			$.ajax({
				type:"POST",
				data:datos,
				url:"../procesos/ventas/agregaProductTemp.php",
				success:function(r){
                   // alert(r); //testeamos
                   //se carga a la tabla para que se vea
                   $('#tablaVentasTempLoad').load("ventas/tablaVentasTemp.php");
				}
			});
		});//cierra el btn agregar venta
        $('#btnVaciarVenta').click(function(){
            //esto es para recargar la tabla no se necesitan de mas cosas en el ajax
			$.ajax({
				url:"../procesos/ventas/vaciarTemp.php",
				success:function(r){
                    $('#tablaVentasTempLoad').load("ventas/tablaVentasTemp.php");
				}
			});
		});//cierra el btn vaciar venta
        
    }); //este cierra el document ready
</script>

<script type="text/javascript">
    function quitarP(index){ //el indice de la sesion
        $.ajax({ //esto es para borrarlo
                type:"POST",
                data:"ind=" + index,
				url:"../procesos/ventas/quitarProducto.php",
				success:function(r){
                    //se recarga la tabla otra vez
                    $('#tablaVentasTempLoad').load("ventas/tablaVentasTemp.php");
                    alertify.success("se quito el producto :D");
				}
			});//termina el ajax
     }//funcion quitar P
     function crearVenta(){
		$.ajax({
			url:"../procesos/ventas/crearVenta.php",
			success:function(r){
				if(r > 0){ //esto nos manda el numero de insercciones para generar la venta en nuestra tabla temporal
                    //se recarga la tabla
					$('#tablaVentasTempLoad').load("ventas/tablaVentasTemp.php");
                    //resetamos el formulario que ocupamos
					$('#frmVentasProductos')[0].reset();
                    //mandamos con exito la compra hecha
					alertify.alert("Venta creada con exito, consulte la informacion de esta en ventas hechas :D");
				}else if(r == 0){ //si nos manda 0 entonces la venta fue vacia
					alertify.alert("No hay lista de venta!!");
				}else{ //y si no error al crear la venta
					alertify.error("No se pudo crear la venta");
				}
			}
		});//termina el ajax
     }//funcion crear venta termina
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#cliente').select2(); //esto es un buscador para mas rapido 
        $('#producto').select2();
    });
</script>