<?php 
//igual hicimos sesiones en esta parte para generar el dinamismo en nuestra pagina que se al instante y no se recarguen
//cosas a cada rato y asi sucesivamente
	session_start();
   // print_r($_SESSION['tablaComprasTemp']); //no se puede poner el mismo nombre de la sesion a 
    //tu archivo php porque marca errores tener cuidado en esa parte puedes tener todo bien pero si la sesion tiene el mismo nombre que el archivo donde me 
    //encuentro sea igual al de la sesion php tiene ese error de no diferenciar entre sesion o archivo .php
 ?>
 <h4>Hacer venta</h4>
 <h4><strong><div id="nombreclienteVenta"></div></strong></h4>
 <table class="table table-bordered table-hover table-condensed" style="text-align: center;">
 	<caption>
 		<span class="btn btn-success" onclick="crearVenta()"> Generar venta
 			<span class="glyphicon glyphicon-usd"></span>
 		</span>
 	</caption>
 	<tr>
 		<td>Nombre</td>
 		<td>Descripcion</td>
 		<td>Precio</td>
 		<td>Cantidad</td>
 		<td>Quitar</td>
 	</tr>
 	<?php 
 	$total=0;//esta variable tendra el total de la compra en dinero
 	$cliente=""; //en esta se guarda el nombre del cliente
 		if(isset($_SESSION['tablaComprasTemp'])): //si la sesion tiene algo entra aqui
 			$i = 0; //este indice es el que nos sirve para quitar en P
 			foreach (@$_SESSION['tablaComprasTemp'] as $key) { //recorremos los datos que traemos en la sesion ponemos @ para evitar datos nulos o warning que nose genere
 				$d = explode("||", @$key); //recordemos que el explode nos quita las divisones algo asi como el split de c+++ u otros lenguajes dividimos por tokems con un delimitador
 	 ?>

 	<tr>
 		<td><?php echo $d[1] ?></td>
 		<td><?php echo $d[2] ?></td>
 		<td><?php echo $d[3] ?></td>
 		<td><?php echo 1; ?></td> <!--siempre es de una cantidad-->
 		<td>
 			<span class="btn btn-danger btn-xs" onclick="quitarP('<?php echo $i; ?>')">
 				<span class="glyphicon glyphicon-remove"></span>
 			</span>
 		</td>
 	</tr>

 <?php 
 		 $total = $total + $d[3]; //hace la suma de los precios
         $i++; //este indice da la vuelta en el for cada vez que se agrega un producto y asi guardamos las posiciones 
         //para ahora si quitarlo dentro de la tabla mandando a llamar esa funcion
 		$cliente = $d[4]; //vamos acumulando igual los nombres de los clientes que se van agregando
 	}
 	endif; 
 ?>

 	<tr> <!--concatenamos para el total-->
 		<td>Total de venta: <?php echo "$".$total; ?></td>
 	</tr>

 </table>
 <script type="text/javascript">
 	$(document).ready(function(){
 		nombre = "<?php echo @$cliente ?>"; //arroba igual por si no existe o nulo
 		$('#nombreclienteVenta').text("Nombre de cliente: " + nombre); //en javascript es con + la concatenacion como en java
 	});
 </script>