<?php
	require_once "../../procesos/ventas/crearReportepdf.php";
	require_once "../../clases/conexion.php";
	require_once "../../clases/ventas.php";

	//echo $_GET['idventa'];
	$idventa = $_GET['idventa'];
	$c = new conectar();
	$conexion = $c->conexion();
	$query = "SELECT vent.fechaCompra,
					 vent.id_venta,
					 vent.id_cliente,
					 art.nombre,
					 art.precio,
					 art.descripcion FROM ventas as vent
					 INNER JOIN articulos as art 
					 on vent.id_producto = art.id_producto
					 and vent.id_venta = '$idventa'";
	$obj = new ventas();
	$result = mysqli_query($conexion,$query);	 
	//P es vertical L horizontal, unidad de medida pt punto, mm milimetri, cm y pulgada in 
	//por ultimo tamaño de la pagina A3, A4 (por defecto), A5, CARTA, PERSONALIZADO x,y ancho y largo
	$pdf = new PDF('P','mm',array(205,205));
	$pdf->AliasNbPages();
	$pdf->AddPage();

	$ver = mysqli_fetch_row($result); //leemos el resultado de la consulta pero el ultimo de toda la lista 

	$fecha = $ver[0];
	$folio = $ver[1];
	$cliente = $obj->nombreCliente($ver[2]);//recordando que estamos en la posicion 3 de la consulta con su id cliente y tenemos nuestra funcion que trai nombre
	if( $cliente == " " ){ $cliente = "No es cliente"; }
	$pdf->SetFillColor(51, 255, 209);
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(185,6,"Fecha: $fecha",1,1,'L',1);
	$pdf->Cell(185,6,"Folio: $folio",1,1,'L',1);
	$pdf->Cell(185,6,"Cliente: $cliente",1,1,'L',1);
	
	
	//Cell(ancho, Alto, texto, borde, salto de linea, alineacion de texto)
	/*Aclaramos que borde puede tomar los valores 1: con border y 0 sin borde.
Salto de linea 1 saltar linea y 0: no saltar linea. En este caso para que sea comprensible hemos identificado el salto de linea, pero debería ir sin texto y sin borde.*/
	$pdf->Cell(46,6,"Nombre de producto",1,0,'L',1);
	$pdf->Cell(46,6,"Precio",1,0,'L',1);
	$pdf->Cell(20,6,"Cantidad",1,0,'L',1);
	$pdf->Cell(70,6,"Descripcion",1,1,'L',1);
	$total = 0;
	//aqui hace algo raro me devuelve todos ultimo registro no lo hace quien sabe xd
	while($mostrar = mysqli_fetch_row($result)){ //esto si se pone en el while funcionabien porque si se pone fuera el fetch tarda mucho y no muestra
		$pdf->Cell(46,6,$mostrar[3],1,0,'L',1);
		$pdf->Cell(46,6,$mostrar[4],1,0,'L',1);
		$pdf->Cell(20,6,"1",1,0,'L',1);
		$pdf->Cell(70,6,$mostrar[5],1,1,'L',1);
		$total += $mostrar[4];	
	}	//aqui si lee el ultimo resultado de la BD
		$pdf->Cell(46,6,$ver[3],1,0,'L',1);
		$pdf->Cell(46,6,$ver[4],1,0,'L',1);
		$pdf->Cell(20,6,"1",1,0,'L',1);
		$pdf->Cell(70,6,$ver[5],1,1,'L',1);
		$total += $ver[4];
	$pdf->Cell(185,6,"Total: $$total",1,1,'L',1);
	$pdf->Output();
?>