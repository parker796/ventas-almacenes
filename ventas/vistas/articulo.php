<?php
    session_start();
    if(isset($_SESSION['usuario'])){ //si el usuario fue exitoso al iniciar sesion
           // echo $_SESSION['usuario']; //no mas para verificar que hace la sesion
?>

<!DOCTYPE html>
<head><title> articulo   </title>
<?php
    require_once "menu.php"; //aqui se agrega el menu
?>
<?php //este es para el select muestre las categorias que se agregaron en esta plantilla de articulos
    require_once "../clases/conexion.php";
    $c = new conectar();
    $conexion = $c->conexion();
    $sql = "SELECT id_categoria,nombreCategoria from categorias"; //mandamos a traer el id y el nombre para el select
    $result = mysqli_query($conexion,$sql);
?>
   </head>

<body>
        <div class="container">
        <h1>Articulos</h1>
        <div class="row">
            <div class="col-sm-4">
                <form id="frmArticulos" ontype="multipart/form-data"> <!--sin esta parte no se puede agregar archivos multipart-->
                     <label>Categoria</label>
                     <select class="form-control input-sm" id="categoriaSelect" name="categoriaSelect"> 
                        <option value="A">Selecciona una categoria</option> <!-- los select tiene por valor de defecto nulo indicado con A-->
                            <?php while($ver = mysqli_fetch_row($result)): //mostramos el nombre que guardamos en la tabla categorias ?>
                                <option value="<?php echo $ver[0] ?>"><?php echo $ver[1]; ?></option>
                            <?php endwhile; ?>
                     </select>
                     <label>Nombre</label>
                     <input type="text" class="form-control input-sm" name="nombre" id="nombre">
                     <label>Descripcion</label>
                     <input type="text" class="form-control input-sm" name="descripcioncantidad" id="descripcioncantidad">
                     <label>Cantidad</label>
                     <input type="text" class="form-control input-sm" name="cantidad" id="cantidad">
                     <label>Precio</label>
                     <input type="text" class="form-control input-sm" name="precio" id="precio">
                     <label>Imagen</label>
                     <input type="file" name="imagen" id="imagen">
                     <p></p>
                     <span class="btn btn-primary" id="btnAgregarArticulo">Agregar</span>

                </form>
            </div>
            <div class="col-sm-8">
                <div id="tablaArticulosLoad"> </div>
            </div>
        </div>
        </div>
 <!-- Button trigger modal -->

		<!-- Modal -->
		<div class="modal fade" id="abremodalUpdateArticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Actualiza articulo</h4>
					</div>
					<div class="modal-body">
                    <form id="frmArticulosU" ontype="multipart/form-data"> <!--sin esta parte no se puede agregar archivos multipart-->
                    <input type="text" id="idarticulo" name="idarticulo" hidden="">
                     <label>Categoria</label>
                     <select class="form-control input-sm" id="categoriaSelectU" name="categoriaSelectU"> 
                        <option value="A">Selecciona una categoria</option> <!-- los select tiene por valor de defecto nulo indicado con A-->
                                <?php 
                                    $sql = "SELECT id_categoria,nombreCategoria from categorias"; //mandamos a traer el id y el nombre para el select
                                    $result = mysqli_query($conexion,$sql);
                                
                                ?>
                            
                            <?php while($ver = mysqli_fetch_row($result)): //mostramos el nombre que guardamos en la tabla categorias ?>
                                <option value="<?php echo $ver[0] ?>"><?php echo $ver[1]; ?></option>
                            <?php endwhile; ?>
                     </select>
                     <label>Nombre</label>
                     <input type="text" class="form-control input-sm" name="nombreU" id="nombreU">
                     <label>Descripcion</label>
                     <input type="text" class="form-control input-sm" name="descripcioncantidadU" id="descripcioncantidadU">
                     <label>Cantidad</label>
                     <input type="text" class="form-control input-sm" name="cantidadU" id="cantidadU">
                     <label>Precio</label>
                     <input type="text" class="form-control input-sm" name="precioU" id="precioU">
                </form>


					</div>
					<div class="modal-footer">
						<button type="button" id="btnActualizaArticulo" class="btn btn-warning" data-dismiss="modal">Actualizar</button>

					</div>
				</div>
			</div>
		</div>

</body>
</html>

<script type="text/javascript">
    function agregaDatosArticulos(idarticulo){
        //usamos un ajax
        //alert("id del articulo es " + idarticulo);
        $.ajax({
                type:"POST",
                data:"idart=" + idarticulo,
                url:"../procesos/articulos/ObtenDatosArticulo.php",
                success:function(r){
                    //aqui no manejamos un if como en los demas si no nos dejara mostrar el JSON
                   // alert(r); //para ver si lo muestra en JSON
                    //agregamos un JSON que proviene de r que nos devuelve los datos de la consulta
                    //tecnica mas limpia para mandarnos los datos aqui no sale el backend
                     dato = jQuery.parseJSON(r);
                     //el que esta en la moneda son los id de los formularios y el val es de la consulta
                    $('#idarticulo').val(dato['id_producto']);
					$('#categoriaSelectU').val(dato['id_categoria']);
					$('#nombreU').val(dato['nombre']);
					$('#descripcioncantidadU').val(dato['descripcion']);
					$('#cantidadU').val(dato['cantidad']);
					$('#precioU').val(dato['precio']);
                        
                    
                }
              
            });
    }
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btnActualizaArticulo').click(function(){
            datos = $('#frmArticulosU').serialize(); //todos son input dentro del formulario pero no editamos imagen por eso se manda normal
				$.ajax({
					type:"POST",
					data:datos,
					url:"../procesos/articulos/actualizaArticulos.php", 
					success:function(r){
						 //console.log(r); //testeamos lo vemos en consola cuando actualizamos
                        if(r == 1){
                            //cargamos nuevamente la tabla
							$('#tablaArticulosLoad').load("articulos/tablaArticulos.php");
							alertify.success("Agregado con exito :D");
						}else{
							alertify.error("Fallo al subir el archivo :(");
						}
					}
				});

        });

    });
</script>

<script type="text/javascript">
//funcion eliminar categoria
    function eliminaArticulo(idArticulo){
        //este lo copiamos de la pagina de alerfify 
        alertify.confirm('¿Desea eliminar este Articulo?', 
            function(){ 
                //copiamos el ajax para actualizar la BD
                $.ajax({
					type:"POST",
					data:"idarticulo=" + idArticulo, //el que esta en comillas ahora es su post
					url:"../procesos/articulos/eliminarArticulo.php", 
					success:function(r){
						//alert(r);
                        if(r==1){
                            $('#tablaArticulosLoad').load("articulos/tablaarticulos.php"); //cargamos la tabla articulos
							alertify.success("Eliminado con exito :)");
						}else{
							alertify.error("no se pudo eliminarr :(");
						}
					}
				});
            }, 
            function(){ alertify.error('Cancelo !') }
            );
    }
</script>
<script type="text/javascript">
    //caragmos los eventos a utilizar 
    $(document).ready(function(){
        $('#tablaArticulosLoad').load("articulos/tablaarticulos.php"); //cargamos la tabla articulos

        $('#btnAgregarArticulo').click(function(){ //leeemos lo que pulsa al agregar
           //que fue el evento click del boton
		   //si por alguna razon hubo un campo vacio retorna un contador 
		   vacios = validarFormVacio('frmArticulos');

			if( vacios > 0 ){
				alertify.alert("Debes de llenar todos los campos");
				return false; //esto es para que no siga el proceso de ajax
				}
                //generamos otro ajax porque con serialize no se puede mandar la imagen
                var formData = new FormData(document.getElementById("frmArticulos"));
				$.ajax({
					url: "../procesos/articulos/insertaArticulos.php",
					type: "post",  //ajax que lleva archivos
					dataType: "html",
					data: formData,
					cache: false,
					contentType: false,
					processData: false,

					success:function(r){
                        //alert(r); //imprimos si si llega la imagen en post
						if(r == 1){
                            //como sabemos refrescamos la tabla con el articulo agregado
							$('#frmArticulos')[0].reset();
							$('#tablaArticulosLoad').load("articulos/tablaArticulos.php");
							alertify.success("Agregado con exito :D");
						}else{
							alertify.error("Fallo al subir el archivo :(");
						}
					}
				});
        });


    });

</script>

<?php

    }else{
       // echo "hola";
        header("location:../index.php"); //esto es para validar la sesion
    }

?>