<?php
    session_start();

    if(isset($_SESSION['usuario'])){ //si el usuario fue exitoso al iniciar sesion
           // echo $_SESSION['usuario']; //no mas para verificar que hace la sesion
?>

<!DOCTYPE html>
<head>
<title>  categoria  </title>  
<?php require_once "menu.php";?>
</head>

<body>
        <div class="container">
        <h1>Categorias</h1>
            <div class="row">
                <div class="col-sm-4">
                <form id="frmCategorias">
                    <label>Categoria</label>
                    <input type="text" class="form-control input-sm" name="categoria" id="categoria">
                    <p></p>
                    <span class="btn btn-primary" id="btnAgregarCategoria">Agregar</span>
                </form>
                </div>
                <div class="col-sm-6">  
                    <div id="tablacategoriaLoad"> </div>
                </div>
            </div>
        </div>

      <!-- Button trigger modal -->

		<!-- Modal -->
		<div class="modal fade" id="actualizaCategoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Actualiza categorias</h4>
					</div>
					<div class="modal-body">
						<form id="frmCategoriaU">
							<input type="text" hidden="" id="idcategoria" name="idcategoria">
							<label>Categoria</label>
							<input type="text" id="categoriaU" name="categoriaU" class="form-control input-sm">
						</form>


					</div>
					<div class="modal-footer">
						<button type="button" id="btnActualizaCategoria" class="btn btn-warning" data-dismiss="modal">Guardar</button>

					</div>
				</div>
			</div>
		</div>
</body>
</html>

<script type="text/javascript">
    //vamos a crear su evento de agregar categoria
    $(document).ready(function(){
        //cargamos la tabla categorias 
        $('#tablacategoriaLoad').load("categorias/tablacategorias.php");

        $('#btnAgregarCategoria').click(function(){ //leeemos lo que pulsa al agregar
           //que fue el evento click del boton
		   //si por alguna razon hubo un campo vacio retorna un contador 
		   vacios = validarFormVacio('frmCategorias');

			if( vacios > 0 ){
				alertify.alert("Debes de llenar todos los campos");
				return false; //esto es para que no siga el proceso de ajax
				}


            datos = $('#frmCategorias').serialize();
            //datos = $('#frmRegistro').serialize();
            $.ajax({
                type:"POST",
                data:datos,
                url:"../procesos/categorias/AgregarCategoria.php",
                success:function(r){
                    if( r == 1){ //si todo estuvo correcto
                        //esto nos permite limpiar el formulario al agregar un registro
                        $('#frmCategorias')[0].reset();
                        //cargamos la tabla categorias 
                        $('#tablacategoriaLoad').load("categorias/tablacategorias.php");
                        alertify.success("Categoria agregada con exito :D"); 
                    }else{
                        alertify.error("no se pudo agregar categoria :(");
                    }
                }
              
            });
        });
    });
    
</script>

<script type="text/javascript">
		$(document).ready(function(){
			$('#btnActualizaCategoria').click(function(){ //lo hacemos por modal llamamos el evento del clik guardar del modal

				datos=$('#frmCategoriaU').serialize(); //el formulario pero es un input 
				$.ajax({
					type:"POST",
					data:datos,
					url:"../procesos/categorias/actualizaCategoria.php", 
					success:function(r){
						if(r==1){
                            $('#tablacategoriaLoad').load("categorias/tablacategorias.php"); //refrescamos la tabla con el camio que hicimos
							alertify.success("Actualizado con exito :)");
						}else{
							alertify.error("no se pudo actualizar :(");
						}
					}
				});
			});
		});
	</script>
<script type="text/javascript">
    //lo agregamos mediante una modal este funcion se manda a llamar en el icono de editar
    function agregaDato(idCategoria,categoria){
        $('#idcategoria').val(idCategoria); //lo preparamos para que este listo
        //llamamos el input de categoriaU
        $('#categoriaU').val(categoria);
    }
//funcion eliminar categoria
    function eliminaCategoria(idCategoria){
        //este lo copiamos de la pagina de alerfify 
        alertify.confirm('¿Desea eliminar esta Categoria?', 
            function(){ 
                //copiamos el ajax para actualizar la BD
                $.ajax({
					type:"POST",
					data:"idCategoria=" + idCategoria, //el que esta en comillas ahora es su post
					url:"../procesos/categorias/eliminarCategoria.php", 
					success:function(r){
						if(r==1){
                            $('#tablacategoriaLoad').load("categorias/tablacategorias.php"); //refrescamos la tabla con el camio que hicimos
							alertify.success("Eliminado con exito :)");
						}else{
							alertify.error("no se pudo eliminarr :(");
						}
					}
				});
            }, 
            function(){ alertify.error('Cancelo !') }
            );
    }
</script>

<?php

    }else{
       // echo "hola";
        header("location:../index.php"); //esto es para validar la sesion
    }

?>