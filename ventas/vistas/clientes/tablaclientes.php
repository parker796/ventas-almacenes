<?php 
    require_once "../../clases/conexion.php";
	$c = new conectar();
	$conexion = $c->conexion();

	$sql="SELECT id_cliente, nombre, apellido, direccion, email, telefono, rfc FROM clientes";
	$result=mysqli_query($conexion,$sql);
?>

<table class="table table-hover table-condesed table-bordered" style="text-align: center;">
<caption><label>Clientes</label></caption>
    <tr>
    <td>Nombre</td>
    <td>Apellido</td>
    <td>Direccion</td>
    <td>Email</td>
    <td>Telefono</td>
    <td>RFC</td>
    <td>Editar</td>
    <td>Eliminar</td>
    </tr>
    <?php 
    while($ver = mysqli_fetch_row($result)): //mientras haya datos en fila iesima que se agrego un dato se va imprimir en la tabla
    ?>
    <tr>
    <td><?php echo $ver[1] ?></td>
    <td><?php echo $ver[2] ?></td>
    <td><?php echo $ver[3] ?></td>
    <td><?php echo $ver[4] ?></td>
    <td><?php echo $ver[5] ?></td>
    <td><?php echo $ver[6] ?></td>
    <td><span class="btn btn-warning btn-xs " data-toggle="modal" data-target="#actualizaClientes" onclick="
     agregaDatosClientes('<?php echo $ver[0] ?>')">
        <span class="glyphicon glyphicon-pencil"> </span>
     </span>

    </td>
    <td><span class="btn btn-danger btn-xs" onclick="eliminaCliente('<?php echo $ver[0] ?>')"> 
        <span class="glyphicon glyphicon-remove"> </span>
    </span>
    </td>
    </tr>
    <?php endwhile; ?>
</table>