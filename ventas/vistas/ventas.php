<?php
    session_start();

    if(isset($_SESSION['usuario'])){ //si el usuario fue exitoso al iniciar sesion
           // echo $_SESSION['usuario']; //no mas para verificar que hace la sesion
?>
<!DOCTYPE html>
<head><title> ventas   </title> 
<?php
            require_once "menu.php";
        ?>
  </head>
<body>
        <div class="container">
            <h1> Venta de productos</h1>
            <div class="row">
                <div class="col-sm-4"> 
                    <span class="btn btn-default" id="btnventaProductos">Vender producto</span>
                    <span class="btn btn-default" id="btnventasHechas">Ventas hechas</span>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12"> 
                    <div id="ventaProductos"> </div>
                    <div id="ventasHechas"> </div>
                </div>
            </div>

        </div>


</body>
</html>

<script type="text/javascript">
    $(document).ready(function(){
        //esto es al presionar un boton se muestre la tabla para cada uno de ellos
        $('#btnventaProductos').click(function(){
            esconderSeccionVenta(); //escondemos la seccion cuando hacemos click al boton
            $('#ventaProductos').load("ventas/ventaProductos.php"); //cargamos la tabla
            $('#ventaProductos').show(); //muestra el contenido   
        });
    $('#btnventasHechas').click(function(){
            esconderSeccionVenta()
            $('#ventasHechas').load("ventas/ventasyReportes.php"); //cargamos la tabla
            $('#ventasHechas').show(); //muestra el contenido       
    });

    });
    //creamos una funcion para esconder la tabla se aplica para el mismo dashboard 
    function esconderSeccionVenta(){
        $('#ventaProductos').hide(); //usamos la propiedad hide para ocultar
        $('#ventasHechas').hide();
    }
</script>

<?php

    }else{
       // echo "hola";
        header("location:../index.php"); //esto es para validar la sesion
    }

?>