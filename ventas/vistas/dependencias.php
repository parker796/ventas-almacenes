

<meta charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> 
<!-- esto de arriba es una dependencia de bootstrap-->
<link rel="stylesheet" type="text/css" href="../librerias/alertifyjs/css/alertify.css">
<link rel="stylesheet" type="text/css" href="../librerias/alertifyjs/css/themes/default.css">
<link rel="stylesheet" type="text/css" href="../librerias/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../librerias/select2/css/select2.css">
<link rel="stylesheet" type="text/css" href="../css/menu.css">

<script src="../librerias/jquery-3.2.1.min.js"></script>
<script src="../librerias/alertifyjs/alertify.js"></script>
<script src="../librerias/bootstrap/js/bootstrap.js"></script>
<script src="../librerias/select2/js/select2.js"></script>
<!--agregamos la dependencia js de funciones para la validacion y corriga el error el categorias o bien se puede agregar
en la cabezera de categorias.php como nosotros queramos pero como ya agregamos el archivo de dependencias se puede aqui
ya se usa globalmente en el proyecto-->
<script src="../js/funciones.js"></script>
