<?php 
    require_once "../../clases/conexion.php";
    $c = new conectar();
    $conexion = $c->conexion(); //el as es para un alias
    //vamos a generar un inner join para sacar el la ruta que guardamos de la imagen agregada en base a su id y mostrala en la tabla, junto con su categoria
    $sql = "SELECT art.nombre, 
                   art.descripcion, 
                   art.cantidad, 
                   art.precio,
                   img.ruta,
                   cat.nombreCategoria,
                   art.id_producto
                   FROM articulos AS art 
                   INNER JOIN imagenes as img
                   on  art.id_imagen = img.id_imagen /*de esta forma que tiene en comun traemos la ruta de la imagen y la mostramos*/
                   INNER JOIN categorias as cat
                   on art.id_categoria = cat.id_categoria 
                   ORDER BY cat.nombreCategoria ASC"; //igual los podemos ordenar de la forma que queramos aqui es por nombre de categoria
    $result = mysqli_query($conexion,$sql);
?>
<table class="table table-hover table-condesed table-bordered" style="text-align: center;">
<caption><label>articulos y productos</label></caption>
    <tr>
    <td>Nombre</td>
    <td>Descripcion</td>
    <td>Cantidad</td>
    <td>Precio</td>
    <td>Imagen</td>
    <td>Categoria</td>
    <td>Editar</td>
    <td>Eliminar</td>
    </tr>
    <?php
    while ($ver=mysqli_fetch_row($result)):
    ?>
    <tr>
    <td><?php echo $ver[0] ?></td>
    <td><?php echo $ver[1] ?></td>
    <td><?php echo $ver[2] ?></td>
    <td><?php echo $ver[3] ?></td>
    <td><?php 
           // echo $ver[4] //esto esta bien esta mandando a traer la ruta pero para que la muestra bien hay que quitar una ruta de ../ porque estamos en vistas pero estamos fuera de una carpeta cuando se carga
            $imgVer = explode( "/", $ver[4]); //un delimitador en comun y la ruta generamos un arreglo de 4
            /*echo*/ $imgRuta = $imgVer[1]."/".$imgVer[2]."/".$imgVer[3]; //con este ya quitamos un ../
            ?>
         <img width="80" height="80" src="<?php echo $imgRuta ?>">   
    </td>
    <td> <?php echo $ver[5] ?></td>
    <td><span data-toggle="modal" data-target="#abremodalUpdateArticulo" class="btn btn-warning btn-xs" 
        onclick="agregaDatosArticulos('<?php echo $ver[6] ?>')">
        <span class="glyphicon glyphicon-pencil"> </span>
     </span>

    </td>
    <td><span class="btn btn-danger btn-xs" onclick="eliminaArticulo('<?php echo $ver[6] ?>')"> 
        <span class="glyphicon glyphicon-remove"> </span>
    </span>
    </td>
    </tr>
    <?php endwhile; ?>
</table>