<?php
    session_start();
    if(isset($_SESSION['usuario'])){ //si el usuario fue exitoso al iniciar sesion
           // echo $_SESSION['usuario']; //no mas para verificar que hace la sesion
?>

<!DOCTYPE html>
<head><title>  clientes  </title>   
<?php
            require_once "menu.php";
        ?>
</head>
<body>

<div class="container">
            <h1>Clientes </h1>
            <div class="row">
                <div class="col-sm-4">
                <form id="frmClientes">
                <label>Nombre</label>
                <input type="text" class="form-control input-sm" name="nombre" id="nombre">
                <label>Apellido</label>
                <input type="text" class="form-control input-sm" name="apellido" id="apellido">
                <label>Direccion</label>
                <input type="text" class="form-control input-sm" name="direccion" id="direccion">
                <label>Email</label>
                <input type="text" class="form-control input-sm" name="email" id="email">
                <label>Telefono</label>
                <input type="text" class="form-control input-sm" name="telefono" id="telefono">
                <label>RFC</label>
                <input type="text" class="form-control input-sm" name="rfc" id="rfc">
                <p></p>
                <span class="btn btn-primary" id="btnagregarClientes">Agregar</span>
                </form>
                </div>
            
                <div class="col-sm-8">   <!-- esta parte es para hacer la tabla mas grande a como nosotros queramos -->
                    <div id="tablaclientesLoad"></div>
                </div>
        </div>
        </div>

         <!-- Button trigger modal -->

		<!-- Modal -->
		<div class="modal fade" id="actualizaClientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Actualiza clientes</h4>
					</div>
					<div class="modal-body">
						<form id="frmClientesU">
							<input type="text" hidden="" id="idcliente" name="idcliente">
							<label>Nombre</label>
                            <input type="text" class="form-control input-sm" name="nombreU" id="nombreU">
                            <label>Apellido</label>
                            <input type="text" class="form-control input-sm" name="apellidoU" id="apellidoU">
                            <label>Direccion</label>
                            <input type="text" class="form-control input-sm" name="direccionU" id="direccionU">
                            <label>Email</label>
                            <input type="text" class="form-control input-sm" name="emailU" id="emailU">
                            <label>Telefono</label>
                            <input type="text" class="form-control input-sm" name="telefonoU" id="telefonoU">
                            <label>RFC</label>
                            <input type="text" class="form-control input-sm" name="rfcU" id="rfcU">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" id="btnActualizaClientes" class="btn btn-warning" data-dismiss="modal">Guardar</button>

					</div>
				</div>
			</div>
		</div>
</body>
</html>

<script type="text/javascript">
    function agregaDatosClientes(idcliente){
        //usamos un ajax
       // alert("id del idcliente es " + idcliente);
        $.ajax({
                type:"POST",
                data:"idcli=" + idcliente,
                url:"../procesos/clientes/ObtenDatosClientes.php",
                success:function(r){
                    //aqui no manejamos un if como en los demas si no nos dejara mostrar el JSON
                   // alert(r); //para ver si lo muestra en JSON
                    //agregamos un JSON que proviene de r que nos devuelve los datos de la consulta
                    //tecnica mas limpia para mandarnos los datos aqui no sale el backend
                     dato = jQuery.parseJSON(r);
                     //el que esta en la moneda son los id de los formularios y el val es de la consulta
                    $('#idcliente').val(dato['id_cliente']);
					$('#nombreU').val(dato['nombre']);
					$('#apellidoU').val(dato['apellido']);
					$('#direccionU').val(dato['direccion']);
					$('#emailU').val(dato['email']);
					$('#telefonoU').val(dato['telefono']);
                    $('#rfcU').val(dato['RFC']);
                }
              
            });
    }
</script>

<script type="text/javascript">
		$(document).ready(function(){
			$('#btnActualizaClientes').click(function(){ //lo hacemos por modal llamamos el evento del clik guardar del modal

				datos = $('#frmClientesU').serialize(); //el formulario pero es un input 
				$.ajax({
					type:"POST",
					data:datos,
					url:"../procesos/clientes/actualizaCliente.php", 
					success:function(r){
                      //  alert(r);//testeamos
						if(r == 1){
                            //recargamos la tabla
                            $('#tablaclientesLoad').load("clientes/tablaclientes.php");
							alertify.success("Actualizado con exito :)");
						}else{
							alertify.error("no se pudo actualizar :(");
						}
					}
				});
			});
		});
	</script>

<script type="text/javascript">
    //funcion eliminar cliente
    function eliminaCliente(idcliente){
        //este lo copiamos de la pagina de alerfify 
        alertify.confirm('¿Desea eliminar esta Categoria?', 
            function(){ 
                //copiamos el ajax para actualizar la BD
                $.ajax({
					type:"POST",
					data:"idcliente=" + idcliente, //el que esta en comillas ahora es su post
					url:"../procesos/clientes/eliminarCliente.php", 
					success:function(r){
                        //alert(r); //testeamos
					    if(r==1){
                            //cargamos la tabla
                            $('#tablaclientesLoad').load("clientes/tablaclientes.php");
                            alertify.success("Eliminado con exito :)");
						}else{
							alertify.error("no se pudo eliminarr :(");
						}
					}
				});
            }, 
            function(){ alertify.error('Cancelo !') }
            );
    }
</script>

<script type="text/javascript">
    //cargamos los eventos a realizar
    $(document).ready(function(){
        //cargamos la tabla
        $('#tablaclientesLoad').load("clientes/tablaclientes.php");

        $('#btnagregarClientes').click(function(){ //leeemos lo que pulsa al agregar
           //que fue el evento click del boton
		   //si por alguna razon hubo un campo vacio retorna un contador 
		   vacios = validarFormVacio('frmClientes');

			if( vacios > 0 ){
				alertify.alert("Debes de llenar todos los campos");
				return false; //esto es para que no siga el proceso de ajax
				}


            datos = $('#frmClientes').serialize();
            //datos = $('#frmRegistro').serialize();
            $.ajax({
                type:"POST",
                data:datos,
                url:"../procesos/clientes/agregarClientes.php",
                success:function(r){
                  // alert(r); //testeamos
                    if( r == 1){ //si todo estuvo correcto
                        //limpiamos el formulario en la columna 0
                        $('#frmClientes')[0].reset();
                        //cargamos la tabla
                        $('#tablaclientesLoad').load("clientes/tablaclientes.php");
                        alertify.success("Cliente agregado con exito :D"); 
                    }else{
                        alertify.error("no se pudo agregar cliente :(");
                    }
                }
              
            });
        });
    });


</script>



<?php

    }else{
       // echo "hola";
        header("location:../index.php"); //esto es para validar la sesion
    }

?>