<?php
    session_start();
    //al agregar el and con la sesion otra vez validamos de que si un usuario x llega entrar y copia la url
    //y la inserta en el navegador aseguramos de que no pueda entrar esto dentro de su usuario en la pagina
    if(isset($_SESSION['usuario']) && $_SESSION['usuario'] == "admid"){ //si el usuario fue exitoso al iniciar sesion o fue hecha
           // echo $_SESSION['usuario']; //no mas para verificar que hace la sesion
?>

<!DOCTYPE html>
<head><title> administrar usuarios   </title> 
<?php
            require_once "menu.php";
        ?>
  </head>
<body>
        <div class="container">
            <h1>Administrar usuarios </h1>
            <div class="row">
                <div class="col-sm-4">
                <form id="frmUsuarios">
                <label>Nombre</label>
                <input type="text" class="form-control input-sm" name="nombre" id="nombre">
                <label>Apellido</label>
                <input type="text" class="form-control input-sm" name="apellido" id="apellido">
                <label>Usuario</label>
                <input type="text" class="form-control input-sm" name="usuario" id="usuario">
                <label>Password</label>
                <input type="text" class="form-control input-sm" name="password" id="password">
                <p></p>
                <span class="btn btn-primary" id="btnagregarUsuario">Agregar</span>
                </form>
                </div>
            
                <div class="col-sm-7">   <!-- esta parte es para hacer la tabla mas grande a como nosotros queramos -->
                    <div id="tablausuariosLoad"></div>
                </div>

        </div>
        </div>

        <!-- Modal -->
		<div class="modal fade" id="actualizaUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Actualiza usuarios</h4>
					</div>
					<div class="modal-body">
                    <form id="frmUsuariosU" ontype="multipart/form-data"> <!--sin esta parte no se puede agregar archivos multipart-->
                    <input type="text" id="idusuario" name="idusuario" hidden="">
                     <label>Nombre</label>
                     <input type="text" class="form-control input-sm" name="nombreU" id="nombreU">
                     <label>Apellido</label>
                     <input type="text" class="form-control input-sm" name="apellidoU" id="apellidoU">
                     <label>Usuario</label>
                     <input type="text" class="form-control input-sm" name="usuarioU" id="usuarioU">
                </form>
					</div>
					<div class="modal-footer">
						<button type="button" id="btnActualizaUsuario" class="btn btn-warning" data-dismiss="modal">Actualizar</button>

					</div>
				</div>
			</div>
		</div>
</body>
</html>

<script type="text/javascript">
    function agregaDatosUsuarios(idusuario){
      /*
        //testeo
        alert("el id del usuario es " + idusuario);
        $('#idusuario').val(idusuario); //esta parte es para imprimirlo en textbox oculto
        */
        //usaremos un ajax y solicitamos los datos en JSON para evitar la visualizacion del backend
        $.ajax({
            type:"POST",
            data:"idusua=" + idusuario,
            url:"../procesos/usuarios/obtenerUsuarios.php",
            success:function(r){
                //testeamos si si nos traia el JSON
                //alert(r); 
                dato = jQuery.parseJSON(r); //traemos los datos codificados en JSON y es una formas mas limpia de traerlos
                $('#idusuario').val(dato['id_usuario']);
                $('#nombreU').val(dato['nombre']);
                $('#apellidoU').val(dato['apellido']);
                $('#usuarioU').val(dato['usuario']);
            }
        });
    }
</script>

<script type="text/javascript">
//este ajax es para la actualizacion de los datos para editar cualquier dato y sea actualizado igual en BD
    $(document).ready(function(){
        $('#btnActualizaUsuario').click(function(){
            datos = $('#frmUsuariosU').serialize(); //todos son input dentro del formulario pero no editamos imagen por eso se manda normal
				$.ajax({
					type:"POST",
					data:datos,
					url:"../procesos/usuarios/actualizaUsuarios.php", 
					success:function(r){
						 //console.log(r); //testeamos lo vemos en consola cuando actualizamos
                        if(r == 1){
                            //cargamos la tabla de usuarios
                            $('#tablausuariosLoad').load("usuarios/tablausuarios.php");
							alertify.success("Agregado con exito :D");
						}else{
							alertify.error("Fallo al subir el archivo :(");
						}
					}
				});

        });

    });
</script>

<script type="text/javascript">
//funcion eliminar categoria
    function eliminaUsuario(idusuario){
        //este lo copiamos de la pagina de alerfify 
        alertify.confirm('¿Desea eliminar este Usuario?', 
            function(){ 
                //copiamos el ajax para actualizar la BD
                $.ajax({
					type:"POST",
					data:"idusua=" + idusuario, //el que esta en comillas ahora es su post
					url:"../procesos/usuarios/eliminarUsuarios.php", 
					success:function(r){
						//alert(r);
                        if(r==1){
                            $('#tablausuariosLoad').load("usuarios/tablausuarios.php");
							alertify.success("Eliminado con exito :)");
						}else{
							alertify.error("no se pudo eliminarr :(");
						}
					}
				});
            }, 
            function(){ alertify.error('Cancelo !') }
            );
    }
</script>

<script type="text/javascript">
    //cargamos los eventos
    $(document).ready(function(){
        //cargamos la tabla de usuarios
        $('#tablausuariosLoad').load("usuarios/tablausuarios.php");

        $('#btnagregarUsuario').click(function(){ //leeemos lo que pulsa al agregar
           //que fue el evento click del boton
		   //si por alguna razon hubo un campo vacio retorna un contador 
		   vacios = validarFormVacio('frmUsuarios');

			if( vacios > 0 ){
				alertify.alert("Debes de llenar todos los campos");
				return false; //esto es para que no siga el proceso de ajax
				}


            datos = $('#frmUsuarios').serialize();
            //datos = $('#frmRegistro').serialize();
            $.ajax({
                type:"POST",
                data:datos,
                url:"../procesos/usuarios/InsertaUsuarios.php",
                success:function(r){
                    if( r == 1){ //si todo estuvo correcto
                        $('#frmUsuarios')[0].reset(); //esto es en el formulario en la columna cero limpia
                         //cargamos la tabla de usuarios
                        $('#tablausuariosLoad').load("usuarios/tablausuarios.php");
                        alertify.success("Usuario agregada con exito :D"); 
                    }else{
                        alertify.error("no se pudo agregar al usuario :(");
                    }
                }
              
            });
        });



    });
</scripT>
<?php

    }else{
       // echo "hola";
        header("location:../index.php"); //esto es para validar la sesion
    }

?>